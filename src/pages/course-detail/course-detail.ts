import {Component} from "@angular/core";
import { NavController, NavParams } from 'ionic-angular';

@Component({
    selector: 'course-detail.scss',
    templateUrl: 'course-detail.html'
})
export class CourseDetail{
    selectedItem: any;

    constructor(public navCtrl: NavController, public navParams: NavParams){
        this.selectedItem = navParams.get('item');
    }
}