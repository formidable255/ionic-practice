import {Component} from "@angular/core";
import { NavController, NavParams } from 'ionic-angular';
import { CourseDetail } from "../course-detail/course-detail";

@Component({
    selector: 'david-list.scss',
    templateUrl: 'david-list.html'
})

export class David {
    icons: string[];
    items: Array<{title: string,  note: string, icon: string, info: string[]}>;

    constructor(public navCtrl: NavController, public navParams: NavParams){
        this.icons = ['laptop', 'book', 'laptop', 'calculator', 'book', 'laptop'];
        let courses = ['Into to AI', 'Computer Projects Practicum 1', 'Android', 'Data communications', 'Algorithm Analysis and Design', 'Object Oreiented Analysis and Design'];
        let courseCode = ['COMP 3981', 'COMP 3900', 'COMP 3717', 'COMP 3721', 'COMP 3760', 'COMP 3711'];
        let courseInfo = [
            ['Introduction', 'Intelligent Agents', 'Solving Problems by Searching I', 	
                'Solving Problems by Searching II', 'Adversarial Search I', 'Adversarial Search II', 'Constraint Satisfaction Problems I', 
                'Constraint Satisfaction Problems II', 'Logical Agents', 'First-Order Logic', 'Learning I', 'Learning II'],
            [
                'Stage 1', 'Stage 2', 'Stage 3','Stage 4', 'Supervisor Consistency Adjustment', 'Weekly Lab Activities', 'Presentation and Demonstration'],
            ['Intro & Android Activities', 'Intents, Constraint Layout & Spinner' , 'Lifecycle & Layouts', 	'Events, Themes & ListView','JSON + Fragments',
                'More Fragments + App bar', 	'Locations & Maps', 	'SQLite',  	'Firebase', 	'Design Support Library', 	'Recycler',' Card Views & Nav Drawers', 'Services'],
            ['Network Models', 'Physical Layer 1', 'Physical Layer 2', 'Digital Transmission', 'Analog Transmission', 'Multiplexing', 'Switching', 'Data Link Layer', 	
            'Data Link Control', 'Network Layer', 'Routing', 'Transport Layer Protocols'],
            ['Algorithmic Efficiency', 'Orders of Growth', 	
                'Brute Force', 'Decrease and Conquer', 	
                'Divide and Conquer', 	
                'Transform and Conquer',	
                'Space/Time Trade-offs', 'Graph Algorithms', 'The Greedy Technique', 	
                'Dynamic Programming', 	
                'Backtracking', 'Branch and Bound' ],
            ['Introduction to Software Design', 'Use Case Diagrams', 'Use Case Descriptions', 'Software Requirement Specifications', 
                'User Interface DomEventsPlugin', 'Class Diagrams', 'Sequence and Collaboration Diagrams', 'Software Design Document',
            'UML in Iterative Development', 'Patents', 'Automated Code Generation', 'Test Driven Development']
        ]
        this.items = [];

        for(let i = 0; i < this.icons.length; i++){
            this.items.push({
                title: courses[i],
                note: courseCode[i],
                icon: this.icons[i],
                info: courseInfo[i]
            })
        }
    }

    itemTapped(event, item){
        this.navCtrl.push(CourseDetail, {
            item:item
        })
    }
}
